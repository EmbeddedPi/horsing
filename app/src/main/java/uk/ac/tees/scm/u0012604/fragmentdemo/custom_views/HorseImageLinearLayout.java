package uk.ac.tees.scm.u0012604.fragmentdemo.custom_views;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import uk.ac.tees.scm.u0012604.fragmentdemo.HorseActivity;
import uk.ac.tees.scm.u0012604.fragmentdemo.R;
import uk.ac.tees.scm.u0012604.fragmentdemo.utilities.ManifestReader;

public class HorseImageLinearLayout extends LinearLayout {

    public final static String TAG = HorseImageLinearLayout.class.getSimpleName();

    private TextView mFileName;
    private ImageView mHorseImage;

    private final RequestQueue queue;

    private Response.Listener<Bitmap> mBitmapResponseListener = new Response.Listener<Bitmap>() {

        @Override
        public void onResponse(final Bitmap bitmap) {
            addHorseImage(bitmap);
        }
    };

    private Response.ErrorListener mBitmapErrorResponseListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, error.getMessage());
        }
    };


    public HorseImageLinearLayout(Context context) {
        this(context, (AttributeSet)null);
    }

    public HorseImageLinearLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorseImageLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public HorseImageLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init();

        queue = Volley.newRequestQueue(context);

        TypedArray ta = context.
                getTheme().
                obtainStyledAttributes(
                        attrs,
                        R.styleable.HorseImageLinearLayout,
                        0,
                        0);
        try {
            final String fileName = ta.getString(R.styleable.HorseImageLinearLayout_fileName);
            if(fileName != null) {
                setFileName(fileName);
            }
        }
        finally {
            ta.recycle();
        }
    }

    private void init() {
        setOrientation(LinearLayout.HORIZONTAL);

        // Progammatically add layout parameters
        //
        setLayoutParams(
                new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT));

    }

    public void setFileName(final CharSequence fileName) {
        if(mFileName == null) {
            mFileName = new TextView(getContext());
            addView(mFileName, new LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 0.40f));
        }
        mFileName.setText(fileName);

        initiateImageFetch(fileName);
    }

    private void initiateImageFetch(final CharSequence fileName) {

        final ImageRequest imageRequest = new ImageRequest(
                HorseActivity.BASE_URL + "/" + fileName,
                mBitmapResponseListener,
                400, 0, null,
                Bitmap.Config.ARGB_8888,
                mBitmapErrorResponseListener
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                final String authz = ManifestReader.lookupManifest(getContext(), "authz");

                HashMap<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Basic " + authz);

                return headers;
            }
        };

        queue.add(imageRequest);

    }

    private void addHorseImage(final Bitmap bitmap) {
        if(mHorseImage == null) {
            mHorseImage = new ImageView(getContext());

            addView(mHorseImage, new LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 0.60f));
        }

        mHorseImage.setImageBitmap(bitmap);

        createNotification(bitmap);
    }

    private void createNotification(final Bitmap bitmap) {

        //Hacky way of generating unique notification number.
        //
        Random random = new Random();
        int notifId = random.nextInt(9999 - 1000) + 1000;

        //intent.putExtra("notificationId", notifId);

        //PendingIntent pendingIntent = PendingIntent.getActivity(getContext(),0, intent,0);

        //final Notification notif = new Notification.Builder(getContext(), HorseActivity.NOTIFICATION_CHANNEL_ID)
//                .setContentTitle("New horse image received!")
//                .setContentText(mFileName.getText())
//                .setSmallIcon(R.drawable.ic_my_notification_icon)
//                .setLargeIcon(bitmap)
//                .setChannelId(HorseActivity.NOTIFICATION_CHANNEL_ID)
//                .setContentIntent(pendingIntent)
//                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());

        Log.i(TAG, "Notification Number: " + notifId);

//        notificationManager.notify(notifId, notif);

    }

}
