package uk.ac.tees.scm.u0012604.fragmentdemo;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.warkiz.widget.IndicatorSeekBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import uk.ac.tees.scm.u0012604.fragmentdemo.custom_views.HorseImageLinearLayout;
import uk.ac.tees.scm.u0012604.fragmentdemo.utilities.ManifestReader;
import uk.ac.tees.scm.u0012604.fragmentdemo.utilities.NotificationConfig;
import uk.ac.tees.scm.u0012604.fragmentdemo.utilities.Reflection;

class PixelateHandlerThread extends HandlerThread {

    private static int count = 0;
    private Handler mHandler;

    public PixelateHandlerThread() {
        super("pixelate-handler-thread-" + count);
        count++;
    }

    @Override
    protected void onLooperPrepared() {
        Log.i("PixelateHandlerThread", "onLooperPrepared()");
        mHandler = new Handler(getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Log.i("PixelateHandlerThread", getName() + ":: The message is: " + msg.what);
            }
        };
    }

    public void postTask(Runnable task) {
        if(mHandler == null) {
            mHandler = new Handler(getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    Log.i("PixelateHandlerThread", getName() + ":: The message is: " + msg.what);
                }
            };
        }

        mHandler.post(task);
    }
}

public class HorseActivity extends AppCompatActivity {

    private static final String TAG = HorseActivity.class.getSimpleName();

    public static final String BASE_URL = "https://scma-intranet.tees.ac.uk/users/u0012604/CIS4034-N/horse_images";

    public static final String NOTIFICATION_CHANNEL_ID = "MY KINGDOM FOR A HORSE";

    private static int CHECK_FUTURE_EVENT = 1;
    private static int RUN_PIXELATE_EVENT = 2;

    private boolean threadSleep = false;

    private Future<Void> mFuture = null;

    private Response.Listener<String> mResponseHandler = new Response.Listener<String>() {

        @Override
        public void onResponse(String response) {

            final String[] fileNames = response.split("\n");

            final LinearLayout ll = findViewById(R.id.horse_listing);

            for(final String fileName : fileNames) {

                Log.i(TAG, "Image: " + fileName);
                //Add a new row
                final HorseImageLinearLayout newHorseImageLinearLayout = new HorseImageLinearLayout(HorseActivity.this);

                newHorseImageLinearLayout.setFileName(fileName);

                ll.addView(newHorseImageLinearLayout);
            }
        }
    };

    private Response.ErrorListener mErrorListener = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, error.getMessage());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horse);

        initFab();

        NotificationConfig.createNotificationChannel(NOTIFICATION_CHANNEL_ID, this);

        //Use the volley library to fetch a list of the horse images
        //
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest horseImageListing =   new StringRequest(
                                                Request.Method.GET,
                                                BASE_URL + "/horse_images.txt",
                                                mResponseHandler,
                                                mErrorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                final String authz = ManifestReader.lookupManifest(HorseActivity.this, "authz");

                final HashMap<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Basic " + authz);

                return headers;
            }
        };

        queue.add(horseImageListing);
    }

    private void initFab() {

        final FloatingActionButton noThreadFabBtn = findViewById(R.id.noThreadFabBtn);
        final FloatingActionButton javaThreadFabBtn = findViewById(R.id.javaThreadFabBtn);
        final FloatingActionButton looperTaskFabBtn = findViewById(R.id.looperTaskFabBtn);

        noThreadFabBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noThreadVersion();
                    }
                }
        );
        javaThreadFabBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        javaThreadVersion();
                    }
                }
        );

        looperTaskFabBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        looperThreadVersion();
                    }
                }
        );

    }



    /*
        noThreadVersion()

        This will render the app unusable until the pixelation process has finished -
        not very desirable!

     */
    private void noThreadVersion() {
        processImages();

        Toast.makeText(this, "No threading at all, in fact this message is too late!", Toast.LENGTH_SHORT).show();
    }

    /*
        javaThreadVersion()

        This uses Future & Callable.  However, to make this work we have to send a message for
        Android's main activity loop to handle.  The message is used to check if the Future
        has finished yet.

     */
    private void javaThreadVersion() {
        // Create a handler to check the state of the long running task
        //
        final Handler handler = new Handler() {
            public void handleMessage(final Message message) {
                if(message.what == CHECK_FUTURE_EVENT) {
                    if(mFuture.isDone()) {
                        Log.i(TAG, "Future Done!!");
                                            }
                    else {
                        Log.i(TAG, "Not done yet!");
                        // Create a message to check the status of the future.
                        final Message m = this.obtainMessage(CHECK_FUTURE_EVENT);
                        this.sendMessageDelayed(m, 500);
                    }
                }
            }
        };

        final ExecutorService executor = Executors.newFixedThreadPool(5);

        // Start the long running task...
        mFuture = executor.submit(new Callable<Void>(){
            @Override
            public Void call() throws Exception {
                processImages();
                return null;
            }

        });

        // Create a message to check the status of the future.
        final Message message = handler.obtainMessage(CHECK_FUTURE_EVENT);
        handler.sendMessageDelayed(message, 500);

        Toast.makeText(this, "Using Java Future/Thread API", Toast.LENGTH_SHORT).show();
    }

    /*
        looperThreadVersion()

        This hooks more nicely into the Android way of doing things.

        TODO: This description needs more work!
     */
    private void looperThreadVersion() {
        final PixelateHandlerThread handlerThread = new PixelateHandlerThread();

        handlerThread.start();

        handlerThread.postTask(
                new Runnable() {

                    @Override
                    public void run() {
                        processImages();
                    }
                }
        );

        Toast.makeText(HorseActivity.this, "Android ThreadHandler API", Toast.LENGTH_SHORT).show();

    }

    private void processImages() {
        final ArrayList<ImageView> imageViews = Reflection.enumerateImageViewChildren(ImageView.class, (ViewGroup)findViewById(R.id.horse_listing));

        final IndicatorSeekBar pixWidthISB = findViewById(R.id.pixelationWidth);
        final IndicatorSeekBar pixHeightISB = findViewById(R.id.pixelationHeight);

        for(final ImageView iv : imageViews) {
            Log.i(TAG, "Processing an ImageView");
            final BitmapDrawable bd = (BitmapDrawable)iv.getDrawable();

            final Bitmap toPixelate = bd.getBitmap().copy(Bitmap.Config.ARGB_8888, true);

            pixelateBitmap(toPixelate, pixWidthISB.getProgress(), pixHeightISB.getProgress());

            iv.setImageBitmap(toPixelate);
        }
    }

    /*
        PixelateBitmap

        Chosen because it is a relatively complex task to complete.
        Even so, it has been deliberately slowed down (Thread.sleep())
        so that the thread takes a long time.
     */

    private void pixelateBitmap(final Bitmap bitmap) {
        pixelateBitmap(bitmap, 10, 10);
    }

    private void pixelateBitmap(final Bitmap bitmap, int pixW, int pixH) {
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final int extraX = width % pixW;
        final int extraY = height % pixH;
        final int widthMinusExtraX = width - extraX;
        final int heightMinusExtraY = height - extraY;
        int actPixW = pixW, actPixH = pixH;
        int pixelsInRegion = pixW * pixH;

        Log.i(TAG, "Pixelating a Bitmap" + extraX + " " + extraY + " " + width + " " + height);

        for(int y = 0; y < height; y += actPixH) {
            if(y >= heightMinusExtraY) {
                actPixH = extraY;
                pixelsInRegion = actPixW * actPixH;
            }

            for(int x = 0 ; x < width; x += actPixW) {
                if(x >= widthMinusExtraX) {
                    actPixW = extraX;
                    pixelsInRegion = actPixW * actPixH;
                }

                //Log.i(TAG, String.format("%d, %d", x, y));
                int avgColour = computeAvgColourForRegion(
                                    bitmap,
                                    x, y,
                                    x + actPixW, y + actPixH,
                                    pixelsInRegion);

                setColourForRegion( bitmap,
                                    avgColour,
                                    x, y,
                                    x + actPixW,
                                    y + actPixH);

            }

            //For the purposes of the threading demonstration, slow the process down...
            //
            deliberatelyTakeTooLong(2);
        }
    }

    private void deliberatelyTakeTooLong(int iterations) {
        if(iterations < 0) return;

        for(int i = 0; i < iterations; i++)
            for(int sillyWait = 0; sillyWait < Integer.MAX_VALUE; sillyWait++)
                ;
    }

    private static int computeAvgColourForRegion(final Bitmap bitmap, int startX, int startY, int endX, int endY, int pixelsInBlock) {

        int r = 0, g = 0, b = 0, a = 0;

        for( ; startY < endY; startY++ ) {
            for ( int x = startX; x < endX; x++) {
                int colour = bitmap.getPixel(x, startY);


                a += ((colour >> 24) & 0xff);
                r += ((colour >> 16) & 0xff);
                g += ((colour >>  8) & 0xff);
                b += ((colour      ) & 0xff);
            }
        }

        a /= pixelsInBlock;
        r /= pixelsInBlock;
        g /= pixelsInBlock;
        b /= pixelsInBlock;

        // Log.i(TAG, "ARGB: [" + a + ", " + r + ", " + g + ", " + b + "]");

        return (((a & 0xff) << 24) |
                ((r & 0xff) << 16) |
                ((g & 0xff) << 8) |
                ((b & 0xff)));

    }

    private static void setColourForRegion(final Bitmap bitmap, int colour, int startX, int startY, int endX, int endY) {
        for( ; startY < endY; startY++ ) {
            //Log.i(TAG, "" + startY);
            for (int x = startX ; x < endX; x++) {
                //Log.i(TAG, "" + x);
                bitmap.setPixel(x, startY, colour);
            }
        }
    }
}
