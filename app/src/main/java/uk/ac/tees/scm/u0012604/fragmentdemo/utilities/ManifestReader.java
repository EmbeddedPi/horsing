package uk.ac.tees.scm.u0012604.fragmentdemo.utilities;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.util.HashMap;

/**
 * Created by u0012604 on 18/02/19.
 */

public class ManifestReader {

    private static ManifestReader instance = null;
    private HashMap<Context, HashMap<String, String>> lookUp;

    private ManifestReader() {
        lookUp = new HashMap<>();
    }

    public final static String lookupManifest(Context context, String key) {
        if(instance == null) {
            instance = new ManifestReader();
        }

        if(instance.lookUp.get(context) == null) {
            instance.lookUp.put(context, new HashMap<String, String>());
        }

        //Have we already read from the manifest?
        final String value = instance.lookUp.get(context).get(key);

        return  value != null ? value : instance.readManifest(context, key);
    }

    private String readManifest(Context context, String key) {
        try {
            final ApplicationInfo ai = context
                    .getPackageManager()
                    .getApplicationInfo(
                            context.getPackageName(),
                            PackageManager.GET_META_DATA
                    );

            final String value = ai.metaData.getString(key);

            if(value != null) {
                lookUp.get(context).put(key, value);
            }

            return value;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

}
